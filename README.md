# README #

### What is this App for? ###

* Display bus stops in Zaragoza

![Basilica_del_Pilar-sunset.jpg](https://bitbucket.org/repo/8zXXG65/images/258046579-Basilica_del_Pilar-sunset.jpg)

### How ist this done? ###
* The app tries to follow the Clean Architecture for android ([more info](https://github.com/android10/Android-CleanArchitecture))
* This app follows the MVVM with data binding.
* The App is divided in three layers:
1. Data layer with a class for each kind of data to retrieve from the Server
2. Domain layer with a class that handles this data in order to be decouple with the rest of the project. Nothing is included at this level. If persistence were required, the Realm db implementation would be done here.
3. View layer with all the classes required by Android for the proper visualization of the project

* It uses Retrofit y OkHttp for the communication with the server.
* Dependency injection with Dagger
* Glide is used for the download of the category and cover icons
* Mockito was used to help with testing

I will be happy to discuss anything about this code in particular, and Android in general.