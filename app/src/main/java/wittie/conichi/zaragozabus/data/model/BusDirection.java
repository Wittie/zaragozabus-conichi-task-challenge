package wittie.conichi.zaragozabus.data.model;

import java.util.List;

class BusDirection {

    List<Integer> stops;
    String from;
    String to;

}
