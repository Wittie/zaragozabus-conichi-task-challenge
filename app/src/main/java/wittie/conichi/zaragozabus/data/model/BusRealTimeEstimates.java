package wittie.conichi.zaragozabus.data.model;

import java.util.List;

public class BusRealTimeEstimates {

    List<BusRealTimeEstimateInfo> estimates;

    public int size() {
        return estimates.size();
    }

    public BusRealTimeEstimateInfo getEstimate(int i) {
        return estimates.get(i);
    }

}
