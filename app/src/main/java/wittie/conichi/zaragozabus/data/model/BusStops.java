package wittie.conichi.zaragozabus.data.model;

import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

public class BusStops {

    private List<BusStopLocation> locations;

    private LinkedTreeMap<String, BusLine> lines;

    public List<BusStopLocation> getLocations() {
        return locations;
    }

    public LinkedTreeMap<String, BusLine> getLines() {
        return lines;
    }

}
