package wittie.conichi.zaragozabus.data;

import android.util.Log;

import java.net.UnknownHostException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import wittie.conichi.zaragozabus.ZaragozaBusApplication;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;

public class BusRealTimeInfoDownloader implements Callback<BusRealTimeEstimates> {

    private static final String TAG = BusStopDownloader.class.getSimpleName();

    @Inject
    EndPointInterface apiService;

    private BusRealTimeInfoDownloaderInterface downloadInterface;

    private int stopId;

    public BusRealTimeInfoDownloader() {
        ZaragozaBusApplication.app().basicComponent().inject(this);
    }

    public void setDownloadInterface(BusRealTimeInfoDownloaderInterface downloadInterface) {
        this.downloadInterface = downloadInterface;
    }

    public void requestRealTimeInfoForStop(int stopId) {
        this.stopId = stopId;
        Log.d(TAG, "Requesting Real Time Info for Bus Stop: " + stopId);
        Call<BusRealTimeEstimates> call = apiService.getRealTimeInfoForStop(stopId);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<BusRealTimeEstimates> call, Response<BusRealTimeEstimates> response) {

        if (downloadInterface == null) {
            Log.d(TAG, "onResponse: No download interface attached");
            return;
        }

        if (response.errorBody() != null) {
            Log.d(TAG, "Error in onResponse: " + response.errorBody().toString());
            return;
        }

        if (response.body() == null) {
            downloadInterface.onFailure("Error: Received an empty body from server");
            return;
        }

        Log.d(TAG, "onResponse: " + response.body());
        downloadInterface.onBusStopsDownloaded(stopId, response.body());
    }

    @Override
    public void onFailure(Call<BusRealTimeEstimates> call, Throwable t) {
        String errorMsg;
        if (t instanceof UnknownHostException) {
            errorMsg = "No Internet: Are you sure you are connected?";
        } else {
            errorMsg = "onFailure: " + t.getLocalizedMessage();
        }

        Log.d(TAG, errorMsg);
        downloadInterface.onFailure(errorMsg);
    }
}
