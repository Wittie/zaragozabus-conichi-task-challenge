package wittie.conichi.zaragozabus.data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStops;

/**
 * Created by pablo on 25/05/17.
 */

public interface EndPointInterface {

    String SERVER = "http://api.dndzgz.com/services/";
    String ENDPOINT_BUS_STOPS = "bus";
    String ENDPOINT_BUS_ID = "bus/{id}";

    @GET(ENDPOINT_BUS_STOPS)
    Call<BusStops> getBusStopList();

    @GET(ENDPOINT_BUS_ID)
    Call<BusRealTimeEstimates> getRealTimeInfoForStop(@Path("id") int stopId);

}
