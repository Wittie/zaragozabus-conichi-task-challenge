package wittie.conichi.zaragozabus.data.model;

import java.util.Locale;

class BusRealTimeEstimateInfo {

    private String line;
    private String direction;
    private double estimate;

    @Override
    public String toString() {
        return String.format(Locale.getDefault(), "%s (%s) - %d min", line, direction, (int) estimate);
    }
}
