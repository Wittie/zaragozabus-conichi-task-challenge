package wittie.conichi.zaragozabus.data.model;

import com.google.gson.internal.LinkedTreeMap;

import java.util.List;

class BusLine {

    String name;
    LinkedTreeMap<Integer, BusDirection> directions;
    List<GPSCoordinate> points;
}
