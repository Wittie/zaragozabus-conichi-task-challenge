package wittie.conichi.zaragozabus.data.model;

import java.util.List;

public class BusStopLocation {

    int id;
    String title;
    String subtitle;
    List<String> lines;
    double lat;
    double lon;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
