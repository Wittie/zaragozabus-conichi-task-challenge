package wittie.conichi.zaragozabus.data;

import wittie.conichi.zaragozabus.data.model.BusStops;

public interface BusStopDownloaderInterface {

    void onBusStopsDownloaded(BusStops downloadedBusStops);

    void onFailure(String errorMsg);

}
