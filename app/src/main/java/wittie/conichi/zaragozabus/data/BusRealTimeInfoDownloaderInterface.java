package wittie.conichi.zaragozabus.data;

import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;

/**
 * Created by pablo on 27/05/17.
 */

public interface BusRealTimeInfoDownloaderInterface {

    void onBusStopsDownloaded(int BusStopId, BusRealTimeEstimates estimates);

    void onFailure(String errorMsg);

}
