package wittie.conichi.zaragozabus;

import javax.inject.Singleton;

import dagger.Component;
import wittie.conichi.zaragozabus.data.BusRealTimeInfoDownloader;
import wittie.conichi.zaragozabus.data.BusStopDownloader;
import wittie.conichi.zaragozabus.presentation.viewmodel.BusStopViewModel;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface BasicComponent {

    void inject(BusStopViewModel viewModel);

    void inject(BusStopDownloader downloader);
    void inject(BusRealTimeInfoDownloader downloader);

}
