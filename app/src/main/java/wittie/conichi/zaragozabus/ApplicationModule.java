package wittie.conichi.zaragozabus;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import wittie.conichi.zaragozabus.data.EndPointInterface;

@Module
public class ApplicationModule {

    private final Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return context;
    }

    @Provides
    @Singleton
    EndPointInterface providesEndPointInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EndPointInterface.SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(EndPointInterface.class);
    }
}
