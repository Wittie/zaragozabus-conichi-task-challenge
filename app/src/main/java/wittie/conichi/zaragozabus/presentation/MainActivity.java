package wittie.conichi.zaragozabus.presentation;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import wittie.conichi.zaragozabus.R;
import wittie.conichi.zaragozabus.data.BusRealTimeInfoDownloader;
import wittie.conichi.zaragozabus.data.BusRealTimeInfoDownloaderInterface;
import wittie.conichi.zaragozabus.data.BusStopDownloader;
import wittie.conichi.zaragozabus.data.BusStopDownloaderInterface;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStops;
import wittie.conichi.zaragozabus.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements BusStopDownloaderInterface,
        BusRealTimeInfoDownloaderInterface {

    private static final String TAG = MainActivity.class.getSimpleName();

    ActivityMainBinding binding;

    BusStopDownloader busStopDownloader = new BusStopDownloader();
    BusRealTimeInfoDownloader busInfoDownloader = new BusRealTimeInfoDownloader();

    final BusStopRecyclerViewAdapter adapter = new BusStopRecyclerViewAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.recyclerview_divider));

        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(dividerItemDecoration);

        busStopDownloader.setDownloadInterface(this);
        busInfoDownloader.setDownloadInterface(this);

        refreshData();
    }

    @Override
    public void onBusStopsDownloaded(BusStops downloadedBusStops) {
        Log.d(TAG, "Downloaded lines: " + downloadedBusStops.getLines().size() + " items.");
        Log.d(TAG, "Downloaded locations: " + downloadedBusStops.getLocations().size() + " items.");
        adapter.setBusStops(downloadedBusStops);

        tryToDownloadNextBusStopInfo();
    }

    private void tryToDownloadNextBusStopInfo() {
        final int stopId = adapter.getNextBusTimeToDownload();
        Log.d(TAG, "tryToDownloadNextBusStopInfo: " + stopId);
        if (stopId >= 0) {
            busInfoDownloader.requestRealTimeInfoForStop(stopId);
        }
    }

    @Override
    public void onBusStopsDownloaded(int busStopId, BusRealTimeEstimates estimates) {
        Log.d(TAG, busStopId + ": downloaded estimates: " + estimates.size() + " items.");
        adapter.setRealTimeInfoForStop(busStopId, estimates);
        tryToDownloadNextBusStopInfo();
    }

    @Override
    public void onFailure(String errorMsg) {
        Log.d(TAG, errorMsg);
        Toast.makeText(this, errorMsg, Toast.LENGTH_SHORT).show();
        adapter.resetInfo();
        showEmptyState();
    }

    public void refreshData() {
        refreshButtonPressed(null);
    }

    public void refreshButtonPressed(View view) {
        busStopDownloader.requestBusStopList();
        hideEmptyState();
    }


    public void showEmptyState() {
        binding.emptyStateView.emptyStateImageView.setVisibility(View.VISIBLE);
        binding.emptyStateView.emptyStateTextView.setVisibility(View.VISIBLE);
        binding.emptyStateView.emptyStateButton.setVisibility(View.VISIBLE);
    }

    public void hideEmptyState() {
        binding.emptyStateView.emptyStateImageView.setVisibility(View.GONE);
        binding.emptyStateView.emptyStateTextView.setVisibility(View.GONE);
        binding.emptyStateView.emptyStateButton.setVisibility(View.GONE);
    }
}
