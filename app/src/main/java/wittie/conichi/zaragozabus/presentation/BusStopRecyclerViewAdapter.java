package wittie.conichi.zaragozabus.presentation;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import wittie.conichi.zaragozabus.R;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStops;

class BusStopRecyclerViewAdapter extends RecyclerView.Adapter<BusStopRecyclerViewViewHolder>  {

    private static final String TAG = BusStopRecyclerViewAdapter.class.getSimpleName();

    private RowInfo[] rowInfos;

    public void setBusStops(BusStops busStops) {
        final int size = busStops.getLocations().size();
        this.rowInfos = new RowInfo[size];

        for (int i = 0; i < size; i++) {
            rowInfos[i] = new RowInfo(busStops.getLocations().get(i));
        }

        notifyDataSetChanged();
    }

    public void setRealTimeInfoForStop(int busStopId, BusRealTimeEstimates estimates) {
        if (rowInfos == null) {
            Log.d(TAG, "setRealTimeInfoForStop: Try to set estimate with null information");
            return;
        }

        final int size = rowInfos.length;
        for (int i = 0; i < size; i++) {
            if (rowInfos[i].getLocationId() == busStopId) {
                rowInfos[i].setEstimates(estimates);
                notifyItemChanged(i);
                return;
            }
        }

        Log.d(TAG, "setRealTimeInfoForStop: No Stop found with id: " + busStopId);
    }

    public void resetInfo() {
        this.rowInfos = null;
        notifyDataSetChanged();
    }

    @Override
    public BusStopRecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_bus_stop, parent, false);
        return new BusStopRecyclerViewViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BusStopRecyclerViewViewHolder holder, int position) {
        if (rowInfos[position].hasEstimates()) {
            holder.bind(rowInfos[position].getLocation(), rowInfos[position].getEstimates());
        } else {
            holder.bind(rowInfos[position].getLocation());
        }

    }

    @Override
    public int getItemCount() {
        return rowInfos != null ? rowInfos.length : 0;
    }

    public int getNextBusTimeToDownload() {
        final int size = rowInfos.length;
        for (int i = 0; i < size; i++) {
            if (!rowInfos[i].hasEstimates()) {
                return rowInfos[i].getLocationId();
            }
        }

        return  -1;
    }
}