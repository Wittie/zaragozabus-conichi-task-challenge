package wittie.conichi.zaragozabus.presentation.viewmodel;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.Locale;

import javax.inject.Inject;

import wittie.conichi.zaragozabus.R;
import wittie.conichi.zaragozabus.ZaragozaBusApplication;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStopLocation;

public class BusStopViewModel {

    @Inject
    Context context;

    private BusStopLocation stopLocation;
    private BusRealTimeEstimates estimates;

    public BusStopViewModel() {
        if (ZaragozaBusApplication.app() != null) {
            ZaragozaBusApplication.app().basicComponent().inject(this);
        }
    }

    public void setBusStopLocation(BusStopLocation busStopLocation) {
        this.stopLocation = busStopLocation;
    }

    public void setEstimates(BusRealTimeEstimates estimates) {
        this.estimates = estimates;
    }

    public String getNumber() {
        return String.format(Locale.getDefault(), "%d.", stopLocation.getId());
    }

    public String getNextBuses() {
        String nextBuses = context.getString(R.string.row_next_buses);
        if (estimates != null) {
            final int size = estimates.size();
            for (int i = 0; i < size; i++) {
                nextBuses += "\n" + estimates.getEstimate(i);
            }
        }

        return nextBuses;
    }

    public String getTitle() {
        return stopLocation.getTitle();
    }

    public String getSubtitle() {
        final int startingIndexOfLines = stopLocation.getSubtitle().indexOf(')') + 1;
        if (startingIndexOfLines > 0) {
            String lines = stopLocation.getSubtitle().substring(startingIndexOfLines);
            return context.getString(R.string.row_bus_lines, lines);
        }

        return stopLocation.getSubtitle();
    }

    @NonNull
    public String getMapsURL() {
        return "http://maps.googleapis.com/maps/api/staticmap?center=" +
                stopLocation.getLat() + "," + stopLocation.getLon() +
                "&zoom=15&size=200x200";
    }

    @NonNull
    Intent getMapsIntent() {
        final String coord = "geo:" + stopLocation.getLat() + "," + stopLocation.getLon();
        return new Intent(Intent.ACTION_VIEW, Uri.parse(coord));
    }

    public boolean onMapLongClick(View view) {
        final Context context = view.getContext();
        Intent mapIntent = getMapsIntent();
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        }

        return true;
    }

    public void onMapClick(final View view) {
        final Context context = view.getContext();
        new AlertDialog.Builder(context)
                .setMessage(R.string.dialog_open_maps_msg)
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_open_maps_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        onMapLongClick(view);
                    }
                })
                .setNegativeButton(R.string.dialog_open_maps_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }
}
