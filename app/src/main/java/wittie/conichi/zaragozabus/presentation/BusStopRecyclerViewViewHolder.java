package wittie.conichi.zaragozabus.presentation;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;

import wittie.conichi.zaragozabus.BR;
import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStopLocation;
import wittie.conichi.zaragozabus.databinding.RowBusStopBinding;
import wittie.conichi.zaragozabus.presentation.viewmodel.BusStopViewModel;

class BusStopRecyclerViewViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding itemBinding;
    private BusStopViewModel viewModel;

    public BusStopRecyclerViewViewHolder(ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        this.itemBinding = itemBinding;
        this.viewModel = new BusStopViewModel();
    }

    public void bind(@NonNull BusStopLocation busStopLocation) {
        bind(busStopLocation, null);
    }

    public void bind(@NonNull BusStopLocation busStopLocation,
                     @Nullable BusRealTimeEstimates estimates) {

        viewModel.setBusStopLocation(busStopLocation);
        viewModel.setEstimates(estimates);
        itemBinding.setVariable(BR.viewModel, viewModel);

        final Context context = itemBinding.getRoot().getContext();
        final RowBusStopBinding busStopBinding = (RowBusStopBinding) this.itemBinding;

        Glide.with(context)
                .load(viewModel.getMapsURL())
                .into(busStopBinding.rowMapImageView);
    }

}
