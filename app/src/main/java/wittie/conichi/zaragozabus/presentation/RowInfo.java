package wittie.conichi.zaragozabus.presentation;

import wittie.conichi.zaragozabus.data.model.BusRealTimeEstimates;
import wittie.conichi.zaragozabus.data.model.BusStopLocation;

/**
 * Created by pablo on 27/05/17.
 */

public class RowInfo {

    private final BusStopLocation location;
    private BusRealTimeEstimates estimates;

    public RowInfo(BusStopLocation busStopLocation) {
        this.location = busStopLocation;
    }

    public void setEstimates(BusRealTimeEstimates estimates) {
        this.estimates = estimates;
    }

    public BusStopLocation getLocation() {
        return location;
    }

    public int getLocationId() {
        return location.getId();
    }

    public boolean hasEstimates() {
        return estimates != null;
    }

    public BusRealTimeEstimates getEstimates() {
        return estimates;
    }
}
