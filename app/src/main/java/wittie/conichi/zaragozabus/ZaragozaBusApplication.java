package wittie.conichi.zaragozabus;

import android.app.Application;

public class ZaragozaBusApplication extends Application {

    private static ZaragozaBusApplication app;
    private BasicComponent basicComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        basicComponent = DaggerBasicComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public static ZaragozaBusApplication app() {
        return app;
    }

    public BasicComponent basicComponent() {
        return basicComponent;
    }
}
