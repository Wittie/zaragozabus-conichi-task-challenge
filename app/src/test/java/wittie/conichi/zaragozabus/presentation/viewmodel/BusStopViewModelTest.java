package wittie.conichi.zaragozabus.presentation.viewmodel;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import wittie.conichi.zaragozabus.data.model.BusStopLocation;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BusStopViewModelTest {

    private static final String BUS_STOP_TITLE = "BUS_STOP_TITLE";
    private static final String BUS_STOP_SUBTITLE_RAW = " (C)Lines: %s";
    private static final String BUS_STOP_SUBTITLE = "Lines: %s";
    private static final int BUS_STOP_ID = 0x00;
    private static final double BUS_STOP_LAT = 0x00;
    private static final double BUS_STOP_LON = 0x00;

    BusStopViewModel busStopViewModel;

    @Mock
    Context context;

    @Mock
    BusStopLocation location;

    @Before
    public void setUp() {
        busStopViewModel = new BusStopViewModel();
        busStopViewModel.context = context;

        when(context.getString(anyInt(), anyString())).thenReturn(BUS_STOP_SUBTITLE);

        when(location.getId()).thenReturn(BUS_STOP_ID);
        when(location.getTitle()).thenReturn(BUS_STOP_TITLE);
        when(location.getSubtitle()).thenReturn(BUS_STOP_SUBTITLE_RAW);
        when(location.getLat()).thenReturn(BUS_STOP_LAT);
        when(location.getLon()).thenReturn(BUS_STOP_LON);
    }

    @Test
    public void setBusStopLocation_shouldSetResultProperlyTitle() {
        busStopViewModel.setBusStopLocation(location);
        assertEquals(BUS_STOP_TITLE, busStopViewModel.getTitle());
    }

    @Test
    public void setBusStopLocation_shouldSetResultProperlySubtitle() {
        busStopViewModel.setBusStopLocation(location);
        assertEquals(BUS_STOP_SUBTITLE, busStopViewModel.getSubtitle());
    }

    @Test
    public void setBusStopLocation_shouldSetResultProperlyBusStopNumber() {
        busStopViewModel.setBusStopLocation(location);
        String expectedValue = BUS_STOP_ID + ".";
        assertEquals(expectedValue, busStopViewModel.getNumber());
    }

    @Test
    public void setBusStopLocation_shouldSetResultProperlyMapsUrl() {
        busStopViewModel.setBusStopLocation(location);
        String expectedURL = "http://maps.googleapis.com/maps/api/staticmap?center=" +
                BUS_STOP_LAT + "," + BUS_STOP_LON + "&zoom=15&size=200x200";
        assertEquals(expectedURL, busStopViewModel.getMapsURL());
    }

}